const api = {

    endpoint: "https://api.openweathermap.org/data/2.5/",
    key: "9cc38bdfa1142a34725e7e80a2ad861e"
};

const input = document.querySelector ("#input");
input.addEventListener("keypress",enter);

function enter(e) {
if (e.keyCode === 13) {
 getInfo(input.value);
}
}

async function getInfo (data) {
const res = await fetch(`${api.endpoint}weather?q=${data}&units=metric&appID=${api.key}`);
const result = await res.json();
displayResult(result);
}
//City
function displayResult (result) {
    let city = document.querySelector("#city");
    city.textContent =`${result.name}, ${result.sys.country}`;

    //Date
getOurDate();
//Temp

let temperature = document.querySelector ("#temperature");
temperature.innerHTML = `${Math.round(result.main.temp)}<span>°C</span>`;

let feelsLike = document.querySelector ("#feelsLike");
feelsLike.innerHTML = "Feels Like" + " " +`${Math.round(result.main.feels_like)}<span>°C</span>`;

let conditions = document.querySelector ("#conditions");
conditions.textContent = `${result.weather[0].main}`;

let variation = document.querySelector("#variation");
variation.innerHTML = "Min:" + `${Math.round(result.main.temp_min)}<span>°C</span>`+ " "+ " " + "Max:" + `${Math.round(result.main.temp_max)}<span>°C</span>`
}

function getOurDate() {
    // Today's date
const myDate = new Date();
const days = ["Sunday", "Monday", "Tuesday", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    //Day
let day = days[myDate.getDay()];
    //Date
let todayDate = myDate.getDate();
    //Month
let month = months[myDate.getMonth()];
    //Year
let year = myDate.getFullYear();

let showDate = document.querySelector("#date");
showDate.textContent = `${day}` + " " +`${todayDate}` + " " + `${month}` + " " + `${year}`;
}





